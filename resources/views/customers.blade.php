
@extends('layouts.app')

@section('title', 'Lista de Clientes')

@section('content')
<div class="container-fluid bg-white p-5 rounded mt-5 mb-5">
    <div class="row justify-content-center align-self-center">
        <div class="col-md-12 text-center">
            <h2 class="mb-4">
                Lista de Clientes
            </h2>
            <div class="row">
                <div class="col-4">
                    <div class="card border-info mb-3">
                        <div class="card-body">
                            <h4 class="card-title">Usuários com Sobrenome</h4>
                            <p class="card-text font-weight-bold">{{ $hasLastNames }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card border-info mb-3">
                        <div class="card-body">
                            <h4 class="card-title">Emails Válidos</h4>
                            <p class="card-text font-weight-bold">{{ $validsEmails }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card border-info mb-3">
                        <div class="card-body">
                            <h4 class="card-title">Usuários com Gênero</h4>
                            <p class="card-text font-weight-bold">{{ $hasGenders }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card border-info mb-3">
                        <div class="card-body">
                            <h4 class="card-title">Usuários sem Sobrenome</h4>
                            <p class="card-text font-weight-bold">{{ $emptyLastNames }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card border-info mb-3">
                        <div class="card-body">
                            <h4 class="card-title">Emails Inválidos</h4>
                            <p class="card-text font-weight-bold">{{ $invalidsEmails }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card border-info mb-3">
                        <div class="card-body">
                            <h4 class="card-title">Usuários sem Gênero</h4>
                            <p class="card-text font-weight-bold">{{ $emptyGenders }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive text-left">
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Sobrenome</th>
                        <th scope="col">Email</th>
                        <th scope="col">Gênero</th>
                        <th scope="col">IP</th>
                        <th scope="col">Empresa</th>
                        <th scope="col">Cidade</th>
                        <th scope="col">Título</th>
                        <th scope="col">Site</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($customers as $customer)
                            <tr>
                                <th scope="row">{{ $customer->id }}</th>
                                <td>{{ $customer->first_name }}</td>
                                <td>{{ $customer->last_name }}</td>
                                <td>{{ $customer->email }}</td>
                                <td>{{ $customer->gender }}</td>
                                <td>{{ $customer->ip_address }}</td>
                                <td>{{ $customer->company }}</td>
                                <td>{{ $customer->city }}</td>
                                <td>{{ $customer->title }}</td>
                                <td> <a href="{{ $customer->website }}" target="_blank">Ir para o site</a> </td>
                            </tr>
                        @endforeach
                    </tbody>
                    {{-- Pagination --}}

                </table>
            </div>
            <div class="row mt-4">
                <div class="col-6 mx-auto">
                    {{ $customers->links() }}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

