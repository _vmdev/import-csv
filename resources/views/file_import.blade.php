@extends('layouts.app')

@section('title', 'Home')

@section('content')

        <div class="container bg-white p-5 rounded mt-5">
            <div class="row justify-content-center align-self-center">
                <div class="col-md-12 text-center">
                    <h2 class="mb-4">
                        Selecione um arquivo CSV e faça importação
                    </h2>
                    <small class="text-muted">1 - Selecione um arquivo no formato CSV</small><br>
                    <small class="text-muted">2 - Clique em Importar </small><br>
                    <form action="{{ route('/customers/import') }}" method="POST" enctype="multipart/form-data" class="mt-4">
                        @csrf
                        <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                            <div class="custom-file text-left">
                                <input type="file" name="file" id="file" class="custom-file-input" id="customFile" accept=".csv" required>
                                <label class="custom-file-label" for="customFile">Selecione um arquivo</label>
                            </div>
                        </div>
                        <button class="btn btn-primary">Importar arquivo</button>
                    </form>
                </div>
            </div>
        </div>


@endsection
