<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>


# Getting started

## :heavy_check_mark: Live Preview

[See Here](https://aba1b2fd233e.ngrok.io/)

## :heavy_check_mark: Dependencies

**Laravel 8.0**
	See Documentation [here](https://laravel.com/docs/8.x/installation)

**Docker**
	See Documentation [here](https://docs.docker.com/get-docker/)

**Docker Compose**
	See Documentation [here](https://docs.docker.com/compose/install/)


## :heavy_check_mark: Installation

:triangular_flag_on_post: Clone the repository

	git clone https://gitlab.com/_vmdev/import-csv

:triangular_flag_on_post: Switch to the repo folder

	cd import-csv

:triangular_flag_on_post: Copy the example env file and make the required configuration changes in the .env file

	cp .env.example .env

:triangular_flag_on_post: Build image with docker-compose

	docker-compose build app

:triangular_flag_on_post: Run Docker enviroment

	docker-compose up -d

:triangular_flag_on_post: Install all the dependencies using composer

	docker-compose exec app composer install

:triangular_flag_on_post: Generate key Illuminate

	docker-compose exec app php artisan key:generate

:triangular_flag_on_post: Run the database migrations (**Set the database connection in .env before migrating**)

	docker-compose exec app php artisan migrate

:triangular_flag_on_post: You can now access the server at http://localhost:8000 :smile:


## :heavy_check_mark: License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
