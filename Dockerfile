FROM php:7.3-fpm

ARG USER
ARG UID
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    libzip-dev zip unzip && \
		docker-php-ext-configure zip --with-libzip && \
		docker-php-ext-install zip && \
		php -m | grep -q 'zip'


RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN useradd -G www-data,root -u 1000 -d /home/big777 big777
RUN mkdir -p /home/big777/.composer && \
    chown -R big777:big777 /home/big777

WORKDIR /var/www

USER big777