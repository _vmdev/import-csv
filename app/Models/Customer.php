<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'email',
        'gender',
        'ip_address',
        'company',
        'city',
        'title',
        'website',
    ];

    static function getHasLastNames()
    {
        $hasLastNames = Customer::whereNotNull('last_name')->count();
        return $hasLastNames;
    }

    static function getValidsEmails()
    {
        $emails = Customer::pluck('email');
        $validsEmails = 0;

        foreach ($emails as $key => $value) {
            if(filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $validsEmails++;
           }
        }
        return $validsEmails;
    }

    static function getHasGenders()
    {
        $hasGender = Customer::whereNotNull('gender')->count();
        return $hasGender;
    }

    static function getEmptyLastNames()
    {
        $emptyLastNames = Customer::whereNull('last_name')->count();
        return $emptyLastNames;
    }

    static function getInvalidsEmails()
    {
        $emails = Customer::pluck('email');
        $invalidsEmails = 0;

        foreach ($emails as $key => $value) {
            if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $invalidsEmails++;
           }
        }
        return $invalidsEmails;
    }

    static function getEmptyGenders()
    {
        $emptyGender = Customer::whereNull('gender')->count();
        return $emptyGender;
    }
}


