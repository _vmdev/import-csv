<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CustomersImport;
use App\Exports\CustomersExport;
use App\Models\Customer;

class CustomerController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileImportExport()
    {
       return back();
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileImport(Request $request)
    {
        Excel::import(new CustomersImport, $request->file('file')->store('temp'));
        return redirect('customers');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileExport()
    {
        return Excel::download(new CustomersExport, 'customers-collection.xlsx');
    }

    public function getAll()
    {
        $customers = Customer::paginate(10);

        $hasLastNames = Customer::getHasLastNames();
        $validsEmails = Customer::getValidsEmails();
        $hasGenders = Customer::getHasGenders();

        $emptyLastNames = Customer::getEmptyLastNames();
        $invalidsEmails = Customer::getInvalidsEmails();
        $emptyGenders = Customer::getEmptyGenders();

        return view('customers', [
                'customers' => $customers,
                'hasLastNames' => $hasLastNames,
                'validsEmails' => $validsEmails,
                'hasGenders' => $hasGenders,
                'emptyLastNames' => $emptyLastNames,
                'invalidsEmails' => $invalidsEmails,
                'emptyGenders' => $emptyGenders
            ]);
    }
}
