<?php

namespace App\Imports;
use App\Models\Customer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;

class CustomersImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Customer([
            'id' => $row[0],
            'first_name' => $row[1],
            'last_name' => $row[2],
            'email' => $row[3],
            'gender' => $row[4],
            'ip_address' => $row[5],
            'company' => $row[6],
            'city' => $row[7],
            'title' => $row[8],
            'website' => $row[9],
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
